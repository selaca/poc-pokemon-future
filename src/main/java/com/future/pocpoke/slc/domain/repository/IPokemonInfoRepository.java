package com.future.pocpoke.slc.domain.repository;

import com.future.pocpoke.slc.domain.model.Pokemon;
import com.future.pocpoke.slc.domain.model.PokemonAttribute;

import java.util.List;
import java.util.Optional;

/**
 * Service interface to support of application use cases
 *
 * @author slc
 * @since 1.0.0
 */
public interface IPokemonInfoRepository {
    /**
     * Gets all Pokemon list
     * in a single request
     * with offset = 0 and limit = -1
     *
     * @return
     * @throws Exception
     */
    public List<Pokemon> getAllPokemons() throws Exception;
    /**
     * Get pokemon selected attribute (height, weight, base_experience)
     * of single Pokemon
     *
     * @param pokemonId
     * @param attrName
     * @return
     * @throws Exception
     */
    public Optional<PokemonAttribute> getPokemonAttributeWithRedGameIndices(String pokemonId, String attrName) throws Exception;
}
