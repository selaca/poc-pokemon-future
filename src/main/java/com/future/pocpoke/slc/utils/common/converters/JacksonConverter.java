package com.future.pocpoke.slc.utils.common.converters;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service to manage Jackson json base converter
 *
 * @author slc
 * @since 1.0.0
 */
@Service
public class JacksonConverter {
    ObjectMapper mapper;

    public ObjectMapper getObjectMapper() {
        return
            Optional.ofNullable( mapper )
                .orElseGet( () -> {
                    this.mapper = new ObjectMapper();
                    return this.mapper;
                });
    }
}
