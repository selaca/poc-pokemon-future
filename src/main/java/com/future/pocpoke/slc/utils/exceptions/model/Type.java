package com.future.pocpoke.slc.utils.exceptions.model;

/**
 *
 */
public enum Type {
    CRITICAL,
    FATAL,
    ERROR,
    WARNING;

    private Type() {}
}
