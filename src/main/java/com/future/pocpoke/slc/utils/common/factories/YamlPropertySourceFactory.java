package com.future.pocpoke.slc.utils.common.factories;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Factory class in order to read file properties en
 * YAML format, not supported by default by Spring
 *
 * @author slc
 * @since 1.0.0
 */
public class YamlPropertySourceFactory implements PropertySourceFactory {

    @Override
    public PropertySource<?> createPropertySource(String s, EncodedResource encodedResource) throws IOException {
        YamlPropertiesFactoryBean factory =
           new YamlPropertiesFactoryBean();

        factory
            .setResources(
                encodedResource.getResource());

        Properties properties = factory.getObject();

        return
            new PropertiesPropertySource(
                encodedResource.getResource().getFilename(),
                properties);
    }
}
