package com.future.pocpoke.slc.infraestructure.controller;

import com.future.pocpoke.slc.infraestructure.controller.dto.PokemonsResponseDTO;
import com.future.pocpoke.slc.utils.exceptions.FunctionalException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Controller advice responsible to manage all excerption produced
 * in controller
 */
@RestControllerAdvice
public class PokemonControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(FunctionalException.class)
    protected ResponseEntity<PokemonsResponseDTO> handleInternalSignController(FunctionalException ex) {
        return
            ResponseEntity
                .status(
                    ex.getHttpStatus().orElseGet( () -> HttpStatus.INTERNAL_SERVER_ERROR))
                .body(
                    PokemonsResponseDTO.builder().errors(ex.getError()).build());
    }
}
