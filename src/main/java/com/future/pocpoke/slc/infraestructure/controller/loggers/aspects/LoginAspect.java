package com.future.pocpoke.slc.infraestructure.controller.loggers.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Aspect to manage logging on methos annotated
 * with annotation @{@link com.future.pocpoke.slc.infraestructure.controller.loggers.annotation.Logging}
 *
 * @author slc
 * @since 1.0.0
 */
@Aspect
@Component
@Slf4j
public class LoginAspect {

    @Around("@annotation(com.future.pocpoke.slc.infraestructure.controller.loggers.annotation.Logging)")
    public Object exceptionsControl(ProceedingJoinPoint joinPoint) throws Throwable {
        Object response;

        try {
            log.info("Invoking Pokemon Highs API method");
            response =
                joinPoint.proceed(joinPoint.getArgs());
            log.info("Invoked Pokemon Highs API method");
        } catch(Exception ex) {
            log.error("Exception when obtaining pokemon highs");
            throw ex;
        }

        return response;
    }

}
