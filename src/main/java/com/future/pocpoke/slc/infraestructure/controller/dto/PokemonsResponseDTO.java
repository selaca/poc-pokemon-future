package com.future.pocpoke.slc.infraestructure.controller.dto;

import com.future.pocpoke.slc.utils.exceptions.model.Error;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Java DTO, representing controller response
 *
 * @author slc
 * @since 1.0.0
 */
@Builder
@Data
public class PokemonsResponseDTO {
   PokemonDTO[] data;
   Error errors;
}
