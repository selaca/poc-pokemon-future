package com.future.pocpoke.slc.infraestructure.controller;

import com.future.pocpoke.slc.application.service.IPokemonHighsService;
import com.future.pocpoke.slc.domain.model.Pokemon;
import com.future.pocpoke.slc.infraestructure.controller.dto.PokemonAttributeDTO;
import com.future.pocpoke.slc.infraestructure.controller.dto.PokemonDTO;
import com.future.pocpoke.slc.infraestructure.controller.dto.PokemonsResponseDTO;
import com.future.pocpoke.slc.infraestructure.controller.loggers.annotation.Logging;
import com.future.pocpoke.slc.utils.exceptions.FunctionalException;
import com.future.pocpoke.slc.utils.exceptions.model.Type;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

@RestController
@RequestMapping("pokemon/v1")
@Api(value = "/pokemon/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@AllArgsConstructor
@Validated
public class PokemonController {
    /** Pokemon highs application service */
    IPokemonHighsService pokemonHighsService;


    @ApiOperation(
        value = "/highs/{attr}",
        notes = "Obtencion de los 5 pokemons con atributo maximo pasado como parámetro, con attributo game_indices.version_name = 'red' ")
    @ApiResponses( {
        @ApiResponse(
            code = 200,
            message = "OK",
            response= PokemonsResponseDTO.class),
        @ApiResponse(
            code = 400,
            message = "Bad request",
            response= PokemonsResponseDTO.class),
        @ApiResponse(
            code = 500,
            message = "Internal server Error",
            response= PokemonsResponseDTO.class) }
    )
    @GetMapping(
        value = "/highs/{attr}",
        produces = { "application/json" })
    @Logging
    public ResponseEntity<PokemonsResponseDTO> gethighs(
            @ApiParam(
                value = "Attributo a determinar e",
                required = true,
                allowableValues = "height, weight, base_experience")
            @PathVariable("attr") String attribute) {

        Optional.ofNullable(attribute)
            .filter( attr -> Stream.of("height", "weight", "base_experience").anyMatch( a -> a.equals(attr)))
            .orElseThrow( () ->
                new FunctionalException(
                    "BadPathException",
                    "Path variable is not suported, suported options are height, weight, base_experience",
                    Type.ERROR,
                    HttpStatus.BAD_REQUEST
            ));

        PokemonDTO[] aPokemonsDTO =
            convertFrom(
                pokemonHighsService.getHighsPokemonWithAttribute(attribute));

        return
            ResponseEntity
                .ok()
                .body(
                    PokemonsResponseDTO.builder().data(aPokemonsDTO).build());

    }

    //  Helper methods ----------------

    private PokemonDTO[] convertFrom(Pokemon[] pokemon) {
        return
            Arrays.asList(pokemon)
                .stream()
                .map( poke ->
                    PokemonDTO.builder()
                        .name(poke.getName())
                        .url(poke.getUrl())
                        .attribute( new PokemonAttributeDTO(poke.getAttribute().get().getName(), poke.getAttribute().get().getValue()))
                        .build() )
                .toArray( size -> new PokemonDTO[size]);
    }

}
