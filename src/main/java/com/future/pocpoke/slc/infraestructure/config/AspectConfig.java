package com.future.pocpoke.slc.infraestructure.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Spring boot aspect configuration
 *
 * @author slc
 * @since 1.0.0
 */
@Configuration
@EnableAspectJAutoProxy
public class AspectConfig {
}
