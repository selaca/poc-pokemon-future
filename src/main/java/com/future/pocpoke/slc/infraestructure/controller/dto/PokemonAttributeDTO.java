package com.future.pocpoke.slc.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Java DTO representing attribute value requested
 *
 * @author slc
 * @since 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PokemonAttributeDTO {
    String name;
    Integer value;
}
