package com.future.pocpoke.slc.infraestructure.repository;

import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.future.pocpoke.slc.domain.model.Pokemon;
import com.future.pocpoke.slc.domain.model.PokemonAttribute;
import com.future.pocpoke.slc.domain.repository.IPokemonInfoRepository;
import com.future.pocpoke.slc.utils.common.converters.JacksonConverter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Repository class.
 * Manage all external request to pokemon API necessary
 * by application
 */
@Repository
@RequiredArgsConstructor
@Setter
public class PokemonInfoRepository implements IPokemonInfoRepository {
    /** Spring reactive rest client */
    WebClient client;
    /** Jackson json marshall/unmarshall service*/
    final JacksonConverter jacksonConverter;

    /**
     * Gets all Pokemon list
     * in a single request
     * with offset = 0 and limit = -1
     *
     * @return
     * @throws Exception
     */
    public List<Pokemon> getAllPokemons() throws Exception {
        // 1. Synchronous Rest srv invocation in order to obtain all list of pokemons
        String jsonResponse =
            externalSyncInvoke(
                uriBuilder ->
                    uriBuilder
                        .path("/pokemon")
                        .queryParam("offset", "0")
                        .queryParam("limit", "-1")
                        .build());

        // 2. Parser to JSONObject and retrieve 'results'
        TreeNode resultNode =
            jacksonConverter.getObjectMapper()
                .readTree(jsonResponse)
                .get("results");

        // 3. Parser results to Pokemon application model
        var lPokemon =
            Arrays.asList(
                jacksonConverter.getObjectMapper()
                    .treeToValue(resultNode, Pokemon[].class))
                .stream()
                .collect(Collectors.toList());

        // 4. Return list of obtained Pokemons
        return lPokemon;
    }

    /**
     * Get pokemon selected attribute (height, weight, base_experience)
     * of single Pokemon
     *
     * @param pokemonId
     * @param attrName
     * @return
     * @throws Exception
     */
    public Optional<PokemonAttribute> getPokemonAttributeWithRedGameIndices(
            String pokemonId, String attrName) throws Exception {
        // 1. Synchronous Rest srv invocation in order to obtain pokemon attributes
        String jsonResponse =
            externalSyncInvoke(
                uriBuilder ->
                    uriBuilder
                        .path("/pokemon/{id}")
                        .build(pokemonId) );

        // 2. Parser to JSONObject
        JsonNode rootNode =
            jacksonConverter.getObjectMapper()
                .readTree(jsonResponse);

        return
            Optional.ofNullable( rootNode )
                // 3. Filter item with attribute $.game_indices.version = "red"
                .filter( this::existPokemonWithRedGameIndicesVersion )
                // 4. Obtain attribute {attrName} if exist
                .map( rootJNode ->
                    PokemonAttribute.builder()
                        .name( attrName )
                        .value( rootJNode.get(attrName).asInt() )
                        .build() );
    }


    // Helper functions --------------------------------------------------------
    private String externalSyncInvoke(Function<UriBuilder, URI> uriBuilder) {
        return
            getClient()
                .method(HttpMethod.GET)
                .uri(uriBuilder)
                .retrieve()
                .toEntity(String.class)
                .block()
                .getBody();
    }

    private boolean existPokemonWithRedGameIndicesVersion(JsonNode rootNode) {
        return
            StreamSupport
                .stream(
                    rootNode.get("game_indices").spliterator(), true)
                .anyMatch( node ->  node.at("/version/name").asText().equals( "red") );
    }



    // Getters / Setters --------------------------------------------------------

    private WebClient getClient() {
        return
            Optional.ofNullable(client)
                .orElseGet( () -> {
                    setClient(
                        WebClient.builder()
                            .baseUrl("https://pokeapi.co/api/v2")
                            .exchangeStrategies(
                                ExchangeStrategies.builder()
                                    .codecs(configurer -> configurer
                                        .defaultCodecs()
                                        .maxInMemorySize(16 * 1024 * 1024)) // Note 16MB of exchange buffer
                                    .build())
                            .build());

                    return this.client;
                });
    }
}
