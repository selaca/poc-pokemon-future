package com.future.pocpoke.slc.infraestructure.controller.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * Configuration class, to manage swagger configuration
 *
 * @author slc
 * @since 1.0.0
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return
            new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.future.pocpoke.slc.infraestructure.controller"))
                .paths(PathSelectors.any())
                .build()
                .groupName("Pokemon")
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return
            new ApiInfo(
                "API Pokemon" ,
                "API de obtencion de atributos máximos de Pokemons",
                "1.0.0",
                "",
                new Contact(
                    "FUT-URE PoC",
                    "http://www.fut-ure.com",
                    ""),
                "",
                "",
                Collections.emptyList());
    }
}
