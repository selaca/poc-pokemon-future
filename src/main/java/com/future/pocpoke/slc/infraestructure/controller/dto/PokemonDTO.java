package com.future.pocpoke.slc.infraestructure.controller.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Java DTO representing business information response
 *
 * @author slc
 * @since 1.0.0
 */
@Builder
@Data
public class PokemonDTO {
    private String name;
    private String url;
    private PokemonAttributeDTO attribute;
}
