package com.future.pocpoke.slc.infraestructure;

import com.future.pocpoke.slc.utils.common.factories.YamlPropertySourceFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Default bootstrap Spring Boot Aplication
 *
 * @author slc
 * @since 1.0.0
 */
@SpringBootApplication(
	scanBasePackages = {"com.future.pocpoke.slc"})
@PropertySource(
	value = "classpath:application.yml",
	factory = YamlPropertySourceFactory.class)
public class PokeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokeApplication.class, args);
	}

}
