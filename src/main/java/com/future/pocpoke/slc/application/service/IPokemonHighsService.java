package com.future.pocpoke.slc.application.service;

import com.future.pocpoke.slc.domain.model.Pokemon;

/**
 * Interface of application use cases
 *
 * @author slc
 * @since 1.0.0
 */
public interface IPokemonHighsService {
    /**
     * Application use cases in order to
     * obtain pokemon highs
     *
     * @param attribute pokemon highs to retrieve
     * @return
     */
    public Pokemon[] getHighsPokemonWithAttribute(String attribute);
}
