package com.future.pocpoke.slc.application.service.impl;

import com.future.pocpoke.slc.application.service.IPokemonHighsService;
import com.future.pocpoke.slc.domain.model.Pokemon;
import com.future.pocpoke.slc.domain.repository.IPokemonInfoRepository;
import com.future.pocpoke.slc.utils.exceptions.FunctionalException;
import com.future.pocpoke.slc.utils.exceptions.model.Type;
import com.future.pocpoke.slc.utils.helpers.lambdas.LambdaHelper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.function.Function;

/**
 * Default implementation
 * of @see {@link IPokemonHighsService}
 *
 *
 * @author slc
 * @since 1.0.0
 */
@Service
@AllArgsConstructor
public class PokemonHighsService implements IPokemonHighsService {
    /** pokemon info repository */
    IPokemonInfoRepository pokemonInfoRepository;

    /**
     * @see IPokemonHighsService#getHighsPokemonWithAttribute(String)
     */
    public Pokemon[] getHighsPokemonWithAttribute(String attribute) {
        try {
            // 1. Retrieve all pokemon list
            var lPokemons =
                pokemonInfoRepository.getAllPokemons();

            var aPokemons =
                lPokemons.parallelStream()
                    // 2. Gets Pokemon attribute if exist
                    .map(
                        getPokemonAttrIfExist(attribute))
                    // 3. Filter only if exist attribute
                    .filter(
                        pokemon -> pokemon.getAttribute().isPresent() )
                    // 4. Sort pokemons by attribute.value descent
                    .sorted(
                        Comparator.comparing(
                            pokemon -> ((Pokemon)pokemon).getAttribute().get().getValue() ).reversed())
                    .limit(5)
                    .toArray(size -> new Pokemon[size]);

            return aPokemons;
        } catch(Exception ex) {
            throw new FunctionalException(
                "ExceptionAppService",
                String.format("Exception obtaining pokemon highs {1} \n: {2} ", attribute, ex.getMessage()),
                Type.ERROR
            );
        }
    }

    // Helper functions ----------------------------------------------------------

    private Function<Pokemon, Pokemon> getPokemonAttrIfExist(String attribute) {
        return
            LambdaHelper.safeFunction(
                pokemon -> {
                    pokemon.setAttribute(
                        pokemonInfoRepository
                            .getPokemonAttributeWithRedGameIndices(
                                pokemon.getUrl().split("/")[pokemon.getUrl().split("/").length - 1],
                                attribute));
                    return pokemon;
                });
    }
}
