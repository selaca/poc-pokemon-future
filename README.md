# FUT-URE PoC Pokemon v1.0.0

PoC carried out as a technical test for a job offer managed by the FUT-URE company

## Requirements

Below we indicate the instructions that must be followed to complete the knowledge test:

In Alea, we are a gaming fanatics and we want to show in JSON via HTTP's request the following scenarios:

1.       The 5 heaviest Pokémons.
2.       The 5 highest Pokémons.
3.       The 5 Pokémons with more base experience.

To know this information, you must create a platform based on micro-services with the following prerequisites:

-  Use Java/SpringBoot
-  JUnit Tests
-  Use PokéAPI: https://pokeapi.co/api/v2/
-  We only want Pokémons of "red version". You can find this information on the section "game_indices" for each Pokémon:
        o version_name = "red"
        o version_url = "https://pokeapi.co/api/v2/version/1/"
-  Please do not use any external library to collect the Pokemon list

It will be valued positively:

-  Clean Code
-  Code Quality
-  Environment running in Dockers
-  SOLID Principles
-  Design Patterns

Our recommendations:

-  https://start.spring.io/
-  Project Lombok
-  h2database

## Prerequisites

- Java 11 installed

- Docker installed

## Installation

To install and test the application, follow the following steps:

1. Clone git repository
   
    <code>git clone https://selaca@bitbucket.org/selaca/poc-pokemon-future.git</code>
   
2. Compile and create image docker:
   
    <code>mvn compile jib:dockerBuild</code>
   
3. Start docker container
   
    <code>docker run -p 8082:8080 -d <docker repository|selaca>/poc-pokemon:1.0.0</code>
   
## Notes

- API Swagger documentation

    http://<server_name>:8082/swagger-ui.html

    